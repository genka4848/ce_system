'use strict';

let gulp = require('gulp'),
	concat = require('gulp-concat'),
	plumber = require('gulp-plumber'),
	sourcemaps = require('gulp-sourcemaps'),
	sass = require('gulp-sass'),
	gulpif = require('gulp-if'),
	uglify = require('gulp-uglify'),
	autoprefixer = require('gulp-autoprefixer'),
	notify = require("gulp-notify"),
	chmod = require('gulp-chmod'),
	uglifycss = require('gulp-uglifycss');

let paths = {
	scripts: {
		assets: ['./src/assets/js/**/*.js'],
		app: ['./src/app/**/*.js']
	}
};

/* Error handler for prod environment */
function prodErrorHandler() {
	process.exit(1);
}

const env = process.env.NODE_ENV || 'dev';

/* assets.js */
gulp.task('assets_js', function() {
	return gulp.src(paths.scripts.assets)
		.pipe(plumber({
			errorHandler: env === 'dev' ? notify.onError("Error: <%= error.message %>") : prodErrorHandler
		}))
		.pipe(gulpif(env === 'dev', sourcemaps.init()))
		.pipe(gulpif(env === 'prod', uglify()))
		.pipe(concat('assets.js'))
		.pipe(gulpif(env === 'dev', sourcemaps.write('./')))
		.pipe(chmod(666))
		.pipe(plumber.stop())
		.pipe(gulpif(env === 'dev', notify({
			message: "ASSETSJS success!",
			onLast: true
		})))
		.pipe(gulp.dest('./public/javascripts'));
});

/* main.js */
gulp.task('app_js', function() {
	return gulp.src(paths.scripts.app)
		.pipe(plumber({
			errorHandler: env === 'dev' ? notify.onError("Error: <%= error.message %>") : prodErrorHandler
		}))
		.pipe(gulpif(env === 'dev', sourcemaps.init()))
		.pipe(gulpif(env === 'prod', uglify()))
		.pipe(concat('main.js'))
		.pipe(gulpif(env === 'dev', sourcemaps.write('./')))
		.pipe(chmod(666))
		.pipe(plumber.stop())
		.pipe(gulpif(env === 'dev', notify({
			message: "MAINJS success!",
			onLast: true
		})))
		.pipe(gulp.dest('./public/javascripts'));
});


/* sass */
gulp.task('sass', function() {
	return gulp.src('./src/assets/sass/*.scss')
		.pipe(plumber({
			errorHandler: env === 'dev' ? notify.onError("Error: <%= error.message %>") : prodErrorHandler
		}))
		.pipe(gulpif(env === 'dev', sourcemaps.init()))
		.pipe(sass.sync().on('error', sass.logError))
		.pipe(gulpif(env === 'prod', uglifycss()))
		.pipe(autoprefixer({
			browsers: ['> 1%', 'last 15 versions'],
			cascade: false
		}))
		.pipe(gulpif(env === 'dev', sourcemaps.write('./')))
		.pipe(chmod(666))
		.pipe(plumber.stop())
		.pipe(gulpif(env === 'dev', notify({
			message: "SASS success!",
			onLast: true
		})))
		.pipe(gulp.dest('./public/stylesheets'));
});

gulp.task('watch', function() {
	gulp.watch([paths.scripts.app], ['app_js']);
	gulp.watch('./src/assets/sass/**/*.scss', ['sass']);
});
gulp.task('js', ['assets_js', 'app_js'], function() {});