var mongoose = require('mongoose');

var schema = mongoose.Schema({
    groupTitle: {
        type: String,
        required: true
    },
    values: {
        type: [{
            title: {
                type: String,
                unique: true,
                required: true
            },
            name: {
                type: String,
                required: true
            },
            coefficient: {
                type: Number,
                min: 0,
                max: 1,
                required: true
            }
        }]
    }
});

schema.statics.getList = function (callback) {
    this.find({}, callback);
};

schema.statics.getItem = function (id, callback) {
    this.find({_id: mongoose.Types.ObjectId(id)}, 'values', callback);
};

schema.statics.getTypes = function (callback) {
    this.find({}, 'values').limit(1).exec(callback);
};

schema.statics.updateById = function (id, params, callback) {
    this.update({_id: id}, {$set: params}, callback);
};

var CategoryModel = mongoose.model('Category', schema, 'categories');

module.exports = CategoryModel;
