var mongoose = require('mongoose');
var crypto = require('crypto');

var userSchema = mongoose.Schema({
    login: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: 'user'
    },
    category: {
        type: String,
        default: 'guest'
    },
    email: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
});

userSchema.virtual('password')
    .set(function (password) {
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function () {
        return this.hashedPassword;
    });

userSchema.methods.encryptPassword = function (password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

userSchema.methods.verifyPassword = function (password) {
    return this.password === this.encryptPassword(password);
};

userSchema.methods.getPublicData = function (password) {
    var publicData = {
            id: this._id,
            name: this.name,
            surname: this.surname,
            email: this.email,
            role: this.role,
            login: this.login,
            category: this.category
    }

    return publicData;
};

var UserModel = mongoose.model('User', userSchema);

module.exports = UserModel;
