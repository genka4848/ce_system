var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var answerItemSchema = new Schema({
	a: {
		type: Number, 
		required: true
	},
	b: {
		type: Number, 
		required: true
	},
	c: {
		type: Number, 
		required: true
	},
	d: {
		type: Number, 
		required: true
	}
}, {_id: false});

var answerSchema = new Schema({
	pollId: {
		type: Schema.Types.ObjectId,
		required: true
	},
	user: {
		id: {
			type: Schema.Types.ObjectId,
			required: true
		},
		fullName: {
			type: String,
			required: true
		},
		category: {
			type: String,
			required: true
		}
	},
	answers: {
		nows: {
			type: [answerItemSchema],
			required: true
		},
		desirables: {
			type: [answerItemSchema],
			required: true
		},
		perspectives: {
			type: [answerItemSchema],
			required: true
		}
	},
	answersAvg: {
		nows: answerItemSchema,
		desirables: answerItemSchema,
		perspectives: answerItemSchema
	},
	year: {
		type: Number,
		required: true
	},
	deep: {
		type: String,
		required: true
	},
	created_at: {
        type: Date,
        default: Date.now()
    }
});

answerSchema.pre('validate', function(next){
	this.year = new Date().getFullYear();

	next();
});

answerSchema.statics.create = function (answer, callback) {
	var newAnswer = new AnswerModel(answer);

	newAnswer.save(function(err) {
		if (err) {
			return callback(err, null);
		}

		return callback(null, newAnswer);
	});
};

answerSchema.statics.getMetaData = function(matches, callback) {
	AnswerModel.aggregate([
		{ $match: matches },
        { 
        	$group: {
	            _id: null,
	            years: { $addToSet: "$year"},
	            total: { $sum: 1 }
        	}
    	},
    	{ 
    		$project: {
	    		_id: 0,
	            years: 1,
	            total: 1
            }
    	}
    ])
    .exec(callback);
};

answerSchema.statics.getAgregateData = function (matches, callback) {

	AnswerModel.aggregate([
        { $match: matches },
        { $group: {
            _id: "$user.category",
            total: { $sum: 1},
            users: {$push: {name: "$user.fullName" , answerId: "$_id"}},

            // now averange
            nowA: { $avg : "$answersAvg.nows.a"},
            nowB: { $avg : "$answersAvg.nows.b"},
            nowC: { $avg : "$answersAvg.nows.c"},
            nowD: { $avg : "$answersAvg.nows.d"},
            
            // desirables averange
            desirablesA: { $avg : "$answersAvg.desirables.a"},
            desirablesB: { $avg : "$answersAvg.desirables.b"},
            desirablesC: { $avg : "$answersAvg.desirables.c"},
            desirablesD: { $avg : "$answersAvg.desirables.d"},
            
            // perspectives averange
            perspectivesA: { $avg : "$answersAvg.perspectives.a"},
            perspectivesB: { $avg : "$answersAvg.perspectives.b"},
            perspectivesC: { $avg : "$answersAvg.perspectives.c"},
            perspectivesD: { $avg : "$answersAvg.perspectives.d"}
        }},
    	{ $project: {
    		_id: 0,
            category: "$_id",
            total: "$total",
            users: "$users",
            nows: {
	                a: "$nowA",
	                b: "$nowB",
	                c: "$nowC",
	                d: "$nowD"
	            },
	            desirables: {
	                a: "$desirablesA",
	                b: "$desirablesB",
	                c: "$desirablesC",
	                d: "$desirablesD"
	            },
	            perspectives: {
	                a: "$perspectivesA",
	                b: "$perspectivesB",
	                c: "$perspectivesC",
	                d: "$perspectivesD"
	            }
    		}
    	}
    ])
   	.exec(callback);
};

answerSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        delete ret['__v'];
    }
});

var AnswerModel = mongoose.model('Answer', answerSchema);

module.exports = AnswerModel;