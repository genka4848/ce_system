var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var questSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    answers: {
        a: {
            type: String,
            default: ''
        },
        b: {
            type: String,
            default: ''
        },
        c: {
            type: String,
            default: ''
        },
        d: {
            type: String,
            default: ''
        }
    }
}, {_id: false});

var pollSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    quests: {
        type: [questSchema],
        required: true
    },
    author: {
        type: String,
        required: true
    },
    public: {
        type: Boolean,
        default: false
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
});

pollSchema.statics.create = function (poll, callback) {
    var newPoll = new PollModel(poll);

    newPoll.save(function (err) {
        if (err) {
            return callback(err, null);
        }

        return callback(null, newPoll);
    })
};

pollSchema.statics.getList = function (callback) {
    this
        .find({})
        .sort('-created_at')
        .exec(callback);
};

pollSchema.statics.get = function (id, callback) {
    this.findById(id, callback);
};

pollSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        delete ret['__v'];
    }
});

var PollModel = mongoose.model('Poll', pollSchema);

module.exports = PollModel;