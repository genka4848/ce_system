var util = require('util');

function RequestParamError(message, statusCode) {
    this.message = message;
    this.statusCode = statusCode;

    Error.captureStackTrace(this, this.constructor);
}

util.inherits(RequestParamError, Error);
RequestParamError.prototype.name = RequestParamError.prototype.constructor.name;

module.exports = RequestParamError;