var passport = require('passport');
var localStrategy = require('passport-local').Strategy;

var User = require('mongoose').model('User');

module.exports.init = function (app) {

    passport.use('local', new localStrategy(
        {
            usernameField: 'login',
            passwordField: 'password'
        },
        function (username, password, done) {
            User.findOne({login: username}, function (err, user) {
                if (err) {
                    return done(err);
                }

                if (!user) {
                    return done(null, false, { message: 'INVALID_LOGIN' });
                }

                if (!user.verifyPassword(password)) {
                    return done(null, false, { message: 'INVALID_PASSWORD' });
                }

                return done(null, user);
            });
        }
    ));

    passport.use('local-sing-up', new localStrategy(
        {
            usernameField: 'login',
            passwordField: 'password',
            passReqToCallback : true
        },
        function (req, login, password, done) {
            User.findOne({login: login}, function (err, user) {
                if (err) {
                    return done(err);
                }

                if (user) {
                    return done(null, false, { message: 'USER_EXISTS' });
                }

                var newUser = new User();
                newUser.login = login;
                newUser.password = password;
                newUser.name = req.body['name'];
                newUser.surname = req.body['surname'];
                newUser.email = req.body['email'];
                newUser.category = req.body['category'];
                
                newUser.save(function (err) {
                    if (err) {
                        return done(err);
                    }

                   return done(null, newUser);
                });
            });
        }
    ));


    // Used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            if (err) return done(err);
            done(err, user);
        });
    });
};
