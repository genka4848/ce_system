var mongoose = require('mongoose');
var nconf = require('nconf');

var dbURI = null;

if (process.env.OPENSHIFT_APP_NAME) {
    dbURI = process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME;
} else {
    var dbPort = nconf.get('db:port') || 27017;
    var dbUrl = nconf.get('db:url') || 'localhost';
    var dbName = nconf.get('db:name') || 'ce';

    dbURI = 'mongodb://localhost:27017/ce';
    dbURI = 'mongodb://' + dbUrl + ':' + dbPort + '/' + dbName;
}

function cleanup() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
}

function init(app) {
    // On connection open successfully
    mongoose.connection.on('connected', function () {
        console.log('Mongoose default connection open to ' + dbURI);
    });

// If the connection throws an error
    mongoose.connection.on('error', function (err) {
        console.log('Mongoose default connection error: ' + err);
    });

// When the connection is disconnected
    mongoose.connection.on('disconnected', function () {
        console.log('Mongoose default connection disconnected');
    });

// If the Node process ends, close the Mongoose connection
    process.on('SIGINT', cleanup);
    process.on('SIGTERM', cleanup);
    process.on('SIGHUP', cleanup);

    mongoose.connect(dbURI);
}

module.exports.init = init;