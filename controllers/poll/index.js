var mongoose = require('mongoose');
var Poll = mongoose.model('Poll');
var RequestParamError = require('../../app_errors/req_param');

var ctrl = {};

/**
 * Get all polls
 * @param req
 * @param res
 * @param next
 */
ctrl.getList = function (req, res, next) {
    Poll.getList(function (err, polls) {
        if (err) {
            return next(err);
        }

        res.json(polls);
    });
};

/**
 * Create new poll
 * @param req
 * @param res
 * @param next
 * @returns {*|{encode, decode, is, equals, pattern}}
 */
ctrl.create = function (req, res, next) {
    if(!req.body.title || !req.body.author || !req.body.quests) {
        return next(new RequestParamError('MISSING_DATA', 400));
    }

    var testData = {
        title: req.body.title,
        author: req.body.author,
        quests: req.body.quests
    };

    Poll.create(testData, function (err, test) {
        if (err) {
            return next(err);
        }

        res.json({success: true, test: test});
    });
};

/**
 * Get polls item by id
 * @param req
 * @param res
 * @param next
 */
ctrl.get = function (req, res, next) {
    if (!req.params.id) {
        return next(new RequestParamError('MISSING_DATA', 400));
    }

    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return next(new RequestParamError('INCORRECT_PARAM_ID', 400));
    }

    Poll.get(req.params.id, function (err, pollDoc) {
        if (err) {
            return next(err);
        }

        res.json({ success: true, poll: pollDoc});
    });

};

module.exports = ctrl;
