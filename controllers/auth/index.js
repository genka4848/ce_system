"use strict";

// Authentication Controller
var passport = require('passport');
var ctrl = {};

/**
 * User local login by passport LocalStrategy
 *
 * @param req
 * @param res
 * @param next
 */
ctrl.logIn = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err);
        }

        if (!user) {
            var err = new Error(info.message);
            err.status = 403;
            return next(err);
        }

        req.logIn(user, function (err) {
            var userData;

            if (err) {
                return next(err);
            }

            userData = user.getPublicData();

            return res.json({success: true, user: userData});
        });
    })(req, res, next);
};

ctrl.singUp = function (req, res, next) {
    passport.authenticate('local-sing-up', function (err, user, info) {
        if (err) {
            return next(err);
        }

        if (!user) {
            var err = new Error(info.message);
            err.status = 403;
            return next(err);
        }

        req.logIn(user, function (err) {
            var userData;

            if (err) {
                return next(err);
            }

            userData = user.getPublicData();

            return res.json({success: true, user: userData});
        });
    })(req, res, next);
};

/**
 * User logout, clear cookie and session data
 *
 * @param req
 * @param res
 * @param next
 */
ctrl.logOut = function (req, res, next) {
    if (!req.isAuthenticated()) {
        res.json({success: false, message: 'NOT_LOGGED'});
        return;
    }
    
    req.logOut();
    req.session.destroy(function (err) {
        if (err) {
            return next(err);
        }
        // TODO add to config name for cookie sid
        res.clearCookie('connect.sid');
        res.json({success: true});
    });
};

/**
 * Get user Data if user is authenticated
 * 
 * @param req
 * @param res
 * @param next
 * @returns {*|{encode, decode, is, equals, pattern}}
 */
ctrl.userInfo = function (req, res, next) {
    var userData;
    
    if (!req.isAuthenticated()) {
        res.json({success: false, message: 'NOT_LOGGED'});
        return;
    }
    
    userData = req.user.getPublicData();

    return res.json({success: true, user: userData});
};

module.exports = ctrl;
