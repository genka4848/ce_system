var mongoose = require('mongoose');
var Answer = mongoose.model('Answer');

var ctrl = {};

ctrl.create = function (req, res, next) {
	Answer.create(req.body, function(err, answer) {
		if (err) {
			return next(err);
		}

		return res.json({success: true, answer: answer})
	});
};

ctrl.agregateData = function (req, res, next) {
	var ans = [],
		select = {
			pollId: mongoose.Types.ObjectId(req.params.id)
		};

	if (req.body.category) {
		select['user.category'] = req.body.category;
	}

	if (req.body.year) {
		select['year'] = parseInt(req.body.year, 10);
	}

	if (req.body.deep) {
		select['deep'] = req.body.deep;
	}

	if (req.body.answers) {		
		ans = req.body.answers.map(function(id) {
			return mongoose.Types.ObjectId(id);
		});

		select['_id'] = {
			$in: ans
		}
	}

	Answer.getAgregateData(select, function(err, result) {
		if (err) {
			return next(err);
		}

		return res.json({success: true, data: result})
	});
};

ctrl.getMetaData = function(req, res, next) {
	var select = {
		pollId: mongoose.Types.ObjectId(req.params.id)
	};
	
	Answer.getMetaData(select, function(err, meta) {
		if (err) {
			return next(err);
		}

		return res.json({success: true, data: meta})
	});
};

module.exports = ctrl;