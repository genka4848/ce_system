var mongoose = require('mongoose');
var Category = mongoose.model('Category');
var RequestParamError = require('../../app_errors/req_param');

var ctrl = {};

/**
 * Get all categories
 * @param req
 * @param res
 * @param next
 */
ctrl.getList = function (req, res, next) {
    Category.getList(function (err, categories) {
        if (err) {
            return next(err);
        }

        res.json(categories);
    });
};

ctrl.getItem = function (req, res, next) {
    Category.getItem(req.params.id, function (err, categories) {
        if (err) {
            return next(err);
        }

        res.json(categories);
    });
};

ctrl.getTypes = function (req, res, next) {
    Category.getTypes(function (err, result) {
        if (err) {
            return next(err);
        }

        res.json(result);
    });
};

/**
 * Update category
 * @param req
 * @param res
 * @param next
 */
ctrl.update = function (req, res, next) {
	if(!req.params.id) {
        return next(new RequestParamError('MISSING_DATA_ID', 400));
    }

    Category.updateById(req.params.id, req.body, function (err) {
        if (err) {
            return next(err);
        }

        res.json(req.body);
    });
};

module.exports = ctrl;
