var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');
var mongoose = require('mongoose');
var nconf = require('nconf');

var models = require('./models');

var app = express();

/**
 * Init app config
 */
nconf.argv()
    .env()
    .file({ file: './loc_config.json' });

/**
 * Init app resources
 */
require('./resources/db').init();
require('./resources/auth').init(app);

// Load app routes
var routes = require('./routes/index');
var auth = require('./routes/auth');
var polls = require('./routes/polls');
var answers = require('./routes/answer');
var userCategory = require('./routes/user-category');

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser('keyboard cat'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'src/templates')));
app.use(session({
    secret: 'keyboard cat',
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    }),
    cookie: { maxAge: 86400000 },
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);
app.use('/', auth);
app.use('/', polls);
app.use('/', answers);
app.use('/', userCategory);

// redirect all others to the index (HTML5 history)
// app.get('*', routes.index);

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('NOT_FOUND');
    err.status = 404;
    next(err);
});

// Error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        var respStatus = err.status || err.statusCode || 500;

        res.status(respStatus);

        if (req.xhr) {
            res.json({
                success: false,
                message: err.message,
                status: respStatus,
                err: err.name
            })
        } else {
            res.render('error', {
                message: err.message,
                error: err
            });
        }
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    var respStatus = err.status || err.statusCode || 500;

    res.status(respStatus);

    if (req.xhr) {
        res.json({
            success: false,
            message: err.message,
            status: respStatus,
            err: err.name
        })
    } else {
        res.render('error', {
            message: err.message,
            error: {}
        });
    }
});

module.exports = app;
