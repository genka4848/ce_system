angular.module('app', ['ui.router', 'ngResource', 'ui.bootstrap', 'ngAnimate', 'poll', 'answer', 'messenger', 'category', 'ceUtil'])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
            $stateProvider
                .state('main', {
                    abstract: true,
                    url: '/',
                    templateUrl: '/home.tpl.html',
                    data: {
                        private: true
                    },
                    controller: ['$scope', '$state', 'AuthService', 'CategoryService',
                    function ($scope, $state, AuthService, CategoryService) {
                        $scope.user = AuthService.getUserData();
                        $scope.categoryTitle = '';
                        $scope.categories = [];

                        $scope.logOut = function () {
                            AuthService.logOut()
                                .then(function () {
                                    $state.go('auth.login');
                                });
                        };

                        $scope.isPrivateRoute = AuthService.isPrivateRoute;

                        CategoryService.getTypes(function(categories) {
                            $scope.categories = categories[0].values;
                            getCategoryTitle($scope.user.category);
                            $scope.categories = [];
                        });

                        function getCategoryTitle(name) {
                            var i, len = $scope.categories.length;

                            for (i = 0; i < len; i++) {
                                if (name === $scope.categories[i].name) {
                                    $scope.categoryTitle = $scope.categories[i].title;
                                    break;
                                }
                            }
                        };
                    }]
                })
                .state('main.default', {
                    url: '',
                    templateUrl: '/default.tpl.html'
                })
                .state('auth', {
                    abstract: true,
                    templateUrl: '/auth_abstract.tpl.html'
                })
                .state('auth.login', {
                    url: '/login',
                    templateUrl: '/login_form.tpl.html',
                    controller: 'SingInController',
                    controllerAs: 'singInCtrl'
                })
                .state('auth.register', {
                    url: '/register',
                    templateUrl: '/register_form.tpl.html',
                    controller: 'SingUpController',
                    controllerAs: 'singUpCtrl',
                    resolve: {
                        categories: ['CategoryService', function(CategoryService) {
                            // return [];
                            return CategoryService.getTypes().$promise;
                        }]
                    }
                });

            $urlRouterProvider.otherwise("/");

            $locationProvider.html5Mode(false);
            $locationProvider.hashPrefix('!');

            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        }])
    .run(['$rootScope', '$state', 'AuthService', function ($rootScope, $state, AuthService) {
        var onAppRun = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            var initState = null;

            event.preventDefault();
            onAppRun();

            AuthService.userStatus()
                .then(function () {
                    if (AuthService.isAuthorized()) {
                        initState = (toState.name == 'auth.login' || toState.name == 'auth.register' || !AuthService.isAccessByRole(toState.data))
                            ? 'main.default' : toState;
                        $state.go(initState);
                    } else {
                        initState = (toState.data && toState.data.private ? 'auth.login' : toState);
                        $state.go(initState);
                    }
                }, function () {
                    $state.go('auth.login');
                });

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (AuthService.isAuthorized()) {
                    if (!toState.data || !toState.data.private || !AuthService.isAccessByRole(toState.data)) {
                        event.preventDefault();
                    }
                } else {
                    if (toState.data && toState.data.private) {
                        event.preventDefault();
                    }
                }

            });
        });
    }]);

angular.module('answer', [])
	.factory('AnswerService', ['$resource', function ($resource) {
		return $resource('answer/:id', {id: '@id'}, {
			getViewData: {
				method: "POST",
				url: '/answer/data/:id',
				params: {
					category: '@category',
					year: "@year",
					deep: "@deep",
					answers: "@answers"
				}
			},
			getMetaData: {
				method: "GET",
				url: "/answer/meta/:id"
			}
		});
	}])
	.config(['$stateProvider', function($stateProvider) {
		$stateProvider
            .state('main.answerView', {
            	abstract: true,
                url: 'answer/view/:pollId',
                templateUrl: '/answer_view_test.tpl.html',
                controller: ['$state', 'meta', function($state, meta) {
                	var self = this;

					self.generalTotal = 0;
					self.years = [];
					self.isCompareMode = false;

                	if (!$state.params.pollId) {
		        		$state.go('main.list');
		        		return;
		        	}

		        	initMetaData();

		        	self.setComareMode = function() {
		        		self.isCompareMode = !self.isCompareMode;
		        	};

		        	function initMetaData() {
			    		var years = [];
			    		
			    		if (!meta) {
			    			return;
			    		}

			    		if(Array.isArray(meta.data) && meta.data[0]) {
			    			years = meta.data[0].years;
			    			self.generalTotal = meta.data[0].total;
			    		}
			    		
			    		years.sort(function(a, b) { return b - a; });

			    		self.years = years;
	    			};
                }],
                controllerAs: 'answerViewAbsCtrl',
                resolve: {
                    groups: ['CategoryService', function(CategoryService) {
                        return CategoryService.query().$promise;
                    }],
                    meta: ['AnswerService', '$stateParams', function(AnswerService, $stateParams) {
                    	if ($stateParams.pollId) {
                    		return AnswerService.getMetaData({id: $stateParams.pollId}).$promise;
                    	}
                        
                        return false;
                    }]
                }
            })
            .state('main.answerView.result', {
                url: '',
                views: {
                	'view1@main.answerView': {
                		templateUrl: '/answer_view.tpl.html',
                		controller: 'AnswerViewContoller',
                		controllerAs: 'answerViewCtrl',
                	},
                	'view2@main.answerView': {
                		templateUrl: '/answer_view.tpl.html',
                		controller: 'AnswerViewContoller',
                		controllerAs: 'answerViewCtrl',
                	}
                }
            })
	}]);
angular.module('app')
    .factory('AuthService', ['$q', '$http', function ($q, $http) {
        var service = {},
            authorized = false,
            userStatusPromise = null,
            user = {};

        service.signIn = function (login, password) {
            var defer = $q.defer();

            $http.post('/login', {
                login: login,
                password: password
            }).success(function (data) {
                if (data.success) {
                    authorized = true;
                    user = data.user;
                }
                defer.resolve(user);
            }).error(function (error) {
                defer.reject(error);
            });

            return defer.promise;
        };

        service.signUp = function (userData) {
            var defer = $q.defer();

            $http.post('/register', userData)
                .success(function (data) {
                if (data.success) {
                    authorized = true;
                    user = data.user;
                }
                defer.resolve(user);
            }).error(function (error) {
                defer.reject(error);
            });

            return defer.promise;
        };

        service.userStatus = function () {
            var defer = $q.defer();

            if(userStatusPromise) {
                return userStatusPromise;
            }

            $http.post('/userinfo').success(function (data) {
                if (data.success) {
                    authorized = true;
                    user = data.user;
                }
                defer.resolve(user);
            }).error(function (error) {
                defer.reject(error);
            });

            userStatusPromise = defer.promise;

            return defer.promise;
        };

        service.logOut = function () {
            var defer = $q.defer();
            
            $http.post('/logout')
                .finally(function (err) {
                    authorized = false;
                    user = {};
                    defer.resolve(err);
                });

            return defer.promise;
        };

        service.isAccessByRole = function(param) {
            if (angular.isDefined(param.onlyAdmin)) {
                return user.role === 'admin';
            }

            return true;
        };

        service.isPrivateRoute = function() {
            return user.role === 'admin';
        };

        service.isAuthorized = function () {
            return authorized;
        };

        service.getUserData = function () {
            return angular.copy(user);
        };

        return service;
    }])
;
angular.module('app')
	.factory('CategoryService', ['$resource', function ($resource) {
		return $resource('category/:id', {id: '@id'}, {
			update: {
				method: 'PUT'
			},
			get: {
				method: 'GET',
				isArray: true
			},
			getTypes: {
				method: 'GET',
				url: '/types',
				isArray: true
			}
		});
	}]);
angular.module('app')
    .controller('SingInController', ['$rootScope', '$state', 'AuthService', function ($rootScope, $state, AuthService) {
        this.login = '';
        this.password = '';

        this.loginUser = function () {
            AuthService.signIn(this.login, this.password)
                .then(function (resp) {
                        $state.go('main.default');
                    },
                    function (err) {
                        $rootScope.systemNotification('Перевірьте введені дані', 'alert-warning', 4000);
                    })
        };
    }]);
angular.module('app')
    .controller('SingUpController', [
        '$rootScope','$state', 'AuthService', 'categories',
        function ($rootScope, $state, AuthService, categories) {
        var self = this;

        self.categories = categories[0] ? categories[0].values : [];
        
        self.user = {
            login: '',
            password: '',
            name: '',
            surname: '',
            email: '',
            category: self.categories[0] ? self.categories[0].name : ''
        };

        self.singUpUser = function () {
            AuthService.signUp(self.user)
                .then(function (data) {
                        $state.go('main.default');
                    },
                    function (self) {
                        $rootScope.systemNotification('Перевірьте введені дані', 'alert-warning', 4000);
                    })
        };
    }]);
angular.module('category', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('main.categories', {
                url: 'categories',
                templateUrl: '/category_groups.tpl.html',
                controller: 'CategoryGroupController',
                controllerAs: 'categoryGroupCtrl',
                data: {
                    private: true,
                    onlyAdmin: true
                },
                resolve: {
                    groups: ['CategoryService', function(CategoryService) {
                        return CategoryService.query().$promise;
                    }]
                }
            })
            .state('main.item', {
                url: 'categories/:id',
                templateUrl: '/categories.tpl.html',
                controller: 'CategoryListController',
                controllerAs: 'categoryListCtrl',
                data: {
                    private: true,
                    onlyAdmin: true
                }
            });
    }]);

angular.module('messenger', [])
    .run(['$rootScope', function($rootScope) {
        $rootScope.systemNotification = function(message, type, delay) {
            if(angular.isDefined(message)) {
                $rootScope.$emit('showNotification', {
                    message: message,
                    type: type,
                    delay: delay
                })
            }
        }
    }])
    .directive('messengerNotification', ['$timeout', function($timeout) {
        return {
            restrict: 'E',
            templateUrl: '/modal/messenger.tpl.html',
            link: function($scope, $element) {
                var timeoutId = null, alert = null, msg = null,
                    defaultConfig = {
                        delay: 3000,
                        type: 'alert-success'
                    };

                alert = angular.element('.alert', $element);
                msg = angular.element('.message', $element);

                $scope.close = function() {
                    if (timeoutId) {
                        $timeout.cancel(timeoutId);
                        timeoutId = null;
                    }
                    alert.slideUp();
                };

                $scope.$on('showNotification', function(event, data) {
                    if (timeoutId) {
                        $scope.close();
                    }
                    $scope.alertType = data.type || defaultConfig.type;
                    msg.text(data.message);
                    alert.slideDown();
                    timeoutId = $timeout($scope.close, data.delay || defaultConfig.delay);
                });
            }
        }
    }]);
angular.module('poll', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('main.list', {
                url: 'list',
                templateUrl: '/polls.tpl.html',
                controller: 'PollsListController',
                controllerAs: 'pollListCtrl'
            })
            .state('main.create', {
                url: 'create',
                templateUrl: '/create_poll.tpl.html',
                controller: 'PollCreateController',
                controllerAs: 'pollCreateCtrl',
                data: {
                    private: true,
                    onlyAdmin: true
                }
            })
            .state('main.pass', {
                url: 'pass/:pollId',
                templateUrl: '/poll_pass.tpl.html',
                controller: 'PollPassController',
                controllerAs: 'pollPassCtrl'
            });
    }]);

angular.module('ceUtil', []);

angular.module('answer')
	.controller('AnswerViewContoller', [
		'$rootScope', '$scope', '$state', 'AnswerService', 'AnswerDeepService', '$timeout', 'groups', 'meta',
		function($rootScope, $scope, $state, AnswerService, AnswerDeepService, $timeout, groups, meta) {
			var self = this,
				// timeout id selecting users ansers
			    timeoutId = null;

		    // answers by category
		    self.answers = [];

		    // all categories
		    self.groups = [];
		    self.group = [];
		    self.categories = [];

		    // special object for define category coeff
		    self.catIds = {};

		    // deep classes of answer changing
		    self.deepClasses = {};

		    // calculated answer with coeff
		    self.answer = {};

		    // users data
		    self.users = [];
		    self.totalFindedUsers = 0;

		    // search params
		    self.pollId = null;
		   	self.category = null;
		    self.year = null;
		    self.deep = null;
		    self.selectedUsers = [];

		    // define needs of reloading users list
		    self.needUsersReload = true;

		    // user filter name val
		    self.filterName = '';

			if (!$state.params.pollId) {
		        return;
		    } 

		    self.groups = groups;
		    self.group = self.groups[0] ? self.groups[0].values : [];
		    self.categories = angular.copy(self.group);

	    	self.pollId = $state.params.pollId;
	    	self.deepClasses = AnswerDeepService.getDeeps();
	    	createCatIds();
	        loadAnswersViewData();
	    

		    self.selectCat = function() {
		    	loadAnswersViewData();
		    };

		    self.selectGroup = function() {
		    	if (self.group) {
		    		self.needUsersReload = false;
		    		self.categories = self.group;
		    		resetAnswer();
		    		createCatIds();
		    		caclWithCoeff();
		    		$scope.$broadcast('updateView', self.answer);
		    	};
		    };


		    self.selectYear = function() {
		    	loadAnswersViewData();
		    };

		    self.selectDeep = function() {
		    	loadAnswersViewData();
		    };

		    self.clearSelect = function() {
			   	self.category = null;
			    self.year = null;
			    self.deep = null;
		    	loadAnswersViewData();
		    };

		    self.selectUsers = function() {
		    	self.needUsersReload = false;
		    	if (timeoutId) {
		    		$timeout.cancel(timeoutId);
		    		timeoutId = null;
		    	}

		    	timeoutId = $timeout(loadAnswersViewData, 700);
		    };

		    function loadAnswersViewData() {
		    	var params = {id: self.pollId};

		    	if (self.needUsersReload) {
	           		self.totalFindedUsers = 0;
	        		self.users = [];
	        		self.selectedUsers = [];
	           	}

		    	if(self.category) {
		    		params.category = self.category
		    	}

		    	if(self.year) {
		    		params.year = self.year;
		    	}

		    	if(self.deep) {
		    		params.deep = self.deep;
		    	}

		    	if (self.selectedUsers.length) {
		    		params.answers = self.selectedUsers;
		    	}

 				resetAnswer();
		        AnswerService.getViewData(params, function (result) {
		            self.answers = result.data;

		            if (self.answers.length) {
	    				caclWithCoeff();
		            	$scope.$broadcast('updateView', self.answer);
	    			} else {
	    				$scope.$broadcast('clearView');
	    			}

		        }, function (err) {
		            $rootScope.systemNotification('Помилка при завантаженні результатів відповідей: ' + data.message, 'alert-danger', 4000);
		        });
	    	};

	    	function createCatIds() {
	    		self.categories.forEach(function(item) {
	    			self.catIds[item.name] = item.coefficient;
	    		});
	    	};

	    	function caclWithCoeff() {
	    		self.answers.forEach(function(item) {

	    			if (self.needUsersReload) {
		           		self.totalFindedUsers  += item.total;
	    				self.users = self.users.concat(item.users);
		           	}

	    			sumGroups('nows', item);
	    			sumGroups('desirables', item);
	    			sumGroups('perspectives', item);
	    		});

	    		averangeGroups();

	    		self.needUsersReload = true;
	    	};

	    	function sumGroups(field, group) {
	    		var elems = self.answer[field],
	    			elemsGroup = group[field];

	    		for(var val in elems) {
	    			elems[val] += elemsGroup[val] * self.catIds[group.category];
	    		}
	    	};

	    	function averangeGroups() {
	    		for(var f in self.answer) {
	    			for(var d in self.answer[f]) {
	    				self.answer[f][d] = Math.round(self.answer[f][d]) / 100;	
	    			}
	    		}
	    	};

	    	function resetAnswer() {
	    		self.answer = {
		    		nows: {
			    		a: 0,
			    		b: 0,
			    		c: 0,
			    		d: 0
			    	},
			    	desirables: {
			    		a: 0,
			    		b: 0,
			    		c: 0,
			    		d: 0
			    	},
			    	perspectives: {
			    		a: 0,
			    		b: 0,
			    		c: 0,
			    		d: 0
			    	}
	    		};
	    	};
	}]);
angular.module('answer')
	.directive('ceAnswerView', [
		function() {
			return {
				restrict: 'E',
				scope: {},
				templateUrl: '/answer_view_directive.tpl.html',
				link: function(scope, element, attrs) {
					var clearData = [null, 0, null, 0, null, 0, null, 0],
						nows = angular.copy(clearData),
						desirables = angular.copy(clearData),
						perspectives = angular.copy(clearData);

					scope.answerInds = {
						a: 1,
						b: 7,
						c: 5,
						d: 3
					};

					scope.$on('updateView', function(event, data) {
						makeData(data);
					});

					scope.$on('clearView', function() {
						chart.series[0].setData(clearData);
						chart.series[1].setData(clearData);
						chart.series[2].setData(clearData);
					});

					function makeData(data) {
						for (var k in data.nows) {
							nows[scope.answerInds[k]] = data.nows[k];
						}

						for (var k in data.desirables) {
							desirables[scope.answerInds[k]] = data.desirables[k];
						}

						for (var k in data.perspectives) {
							perspectives[scope.answerInds[k]] = data.perspectives[k];
						}

						chart.series[0].setData(nows.splice(0, nows.length));
						chart.series[1].setData(desirables.splice(0, desirables.length));
						chart.series[2].setData(perspectives.splice(0, perspectives.length));
					};

					$('.answer-view-container', element).highcharts({
						chart: {
							polar: true,
							type: 'area'
						},
						title: false,
						credits: {
							enabled: false
						},
						pane: {
							startAngle: 0
						},
						xAxis: {
							categories: ['Гнучкість', '(A) Кланова, гнучка модель розвитку', 'Зовнішній', '(D) Ієрархія, консервативна модель розвитку', 'Контроль', '(C) Ринок, експансивна модель розвитку', 'Внутрішній', '(B) Адхократія, синергічна модель розвитку'],
							tickmarkPlacement: 'on',
							lineColor: '#4682B4',
							lineWidth: 1,
							gridLineColor: '#4682B4'
						},
						yAxis: {
							min: 0,
							max: 1,
							tickPositions: [0, 0.33, 0.67, 1],
							gridLineColor: '#4682B4'
						},
						tooltip: {
							shared: true
						},
						series: [{
							type: 'area',
							name: 'Зараз',
							connectEnds: true,
							connectNulls: true,
							fillOpacity: 0.15,
							pointPlacement: 'on',
							marker: {
								symbol: 'circle'
							},
							data: nows
						}, {
							type: 'area',
							name: 'Майбутнє',
							connectEnds: true,
							connectNulls: true,
							fillOpacity: 0.15,
							pointPlacement: 'on',
							marker: {
								symbol: 'circle'
							},
							data: desirables
						}, {
							type: 'area',
							name: 'Перспектива',
							connectEnds: true,
							connectNulls: true,
							fillOpacity: 0.15,
							pointPlacement: 'on',
							marker: {
								symbol: 'circle'
							},
							data: perspectives
						}]
					});

					var chart = $('.answer-view-container', element).highcharts()

					// end link
				}
			}
		}
	]);
angular.module('category')
	.controller('CategoryGroupController', ['groups', function (groups) {
		var self = this;

		self.groups = groups;
	}]);
angular.module('category')
	.controller('CategoryListController', ['CategoryService', '$state', '$uibModal',
        function (CategoryService, $state, $uibModal) {

		var self = this;

		self.categories = []
        self.sumCoeff = 0;

        if (!$state.params.id) {
            $state.go('main.categories');
            return;
        }

        CategoryService.get({id: $state.params.id}, function(data) {
            self.categories = data[0].values;
            updateCoeff();
        });

		self.edit = function(index) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/modal/edit_category.tpl.html',
                controller: 'ModalEditCategoryCtrl',
                resolve: {
                    catIndex: function () {
                        return index;
                    },
                    categories: function() {
                        return self.categories;
                    },
                    calcCoeff: function() {
                        return calcCoeff;
                    },
                    groupId: function() {
                        return $state.params.id;
                    }
                }
            });

            modalInstance.result.then(function (category) {
	            self.categories[index] = category;
                updateCoeff();
			});
		};

        function updateCoeff() {
            self.sumCoeff = calcCoeff(self.categories);
        };

        function calcCoeff(categories) {
            var sum = 0;

            categories.forEach(function(cat) {
                sum += parseFloat(cat.coefficient);
            });

            sum = Math.round(sum * 1000) / 1000;

            return sum;
        };
	}]);
angular.module('category')
    .controller('ModalEditCategoryCtrl',['$scope', '$uibModalInstance', 'CategoryService', 'catIndex', 'categories', 'calcCoeff', 'groupId',
        function ($scope, $uibModalInstance, CategoryService, catIndex, categories, calcCoeff, groupId) {

        $scope.category = angular.copy(categories[catIndex]);
        $scope.categories = angular.copy(categories);

        $scope.ok = function () {
            var coef = null;


            if ($scope.editCategoryForm.$invalid) {
                return;
            }

            coef = parseFloat($scope.category.coefficient);
            if ((coef != $scope.category.coefficient) || coef < 0 || coef > 1) {
                return;
            }

            $scope.categories[catIndex] = $scope.category;

            if (!checkCoeff($scope.categories)) {
                $scope.systemNotification('Сума коефіцієнтів не має перевищувати 1', 'alert-danger', 4000);
                return;
            }

            CategoryService.update({id: groupId}, {
                values: $scope.categories
            }, function(){
                $uibModalInstance.close($scope.category);
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function checkCoeff(categories) {
            return calcCoeff(categories) <= 1;
        };
    }]);
angular.module('poll')
    .controller('ModalInstanceCtrl',['$scope',  '$uibModalInstance', 'item', function ($scope, $uibModalInstance, item) {
        $scope.question = {
            title: '',
            answers: {
                a: '',
                b: '',
                c: '',
                d: ''
            }
        };
        
        if (item) {
            $scope.question = angular.copy(item);
        }
        
        $scope.ok = function () {
            if($scope.createQuestForm.$valid) {
                $uibModalInstance.close($scope.question);
            } else {
                $scope.systemNotification('Дані невірні', 'alert-warning', 4000);
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);
angular.module('poll')
    .controller('PollCreateController', ['$rootScope', '$uibModal', '$state', 'PollService', 'AuthService',
        function ($rootScope, $uibModal, $state, PollService, AuthService) {

            var self = this;

            self.poll = {
                title: '',
                quests: []
            };

            self.editItem = null;
            self.editItemIndex = null;

            self.edit = function (index) {
                if (isQuesIndexExists(index)) {
                    self.editItem = self.poll.quests[index];
                    self.editItemIndex = index;

                    self.addQuestion();
                }
            };

            self.removeQuest = function (index) {
                if (isQuesIndexExists(index)) {
                    self.poll.quests.splice(index, 1);
                }
            };

            self.addQuestion = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/modal/create_quest.tpl.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        item: function () {
                            return self.editItem;
                        }
                    }
                });

                modalInstance.result.then(function (question) {
                    if (self.editItem) {
                        self.poll.quests[self.editItemIndex] = question;
                        clearEditItemData();
                    } else {
                        self.poll.quests.push(question);
                    }

                }, function () {
                    if (self.editItem) {
                        clearEditItemData();
                    }
                });
            };

            // TODO: add from validation, error classes
            self.save = function () {
                if (self.pollCreateForm.$invalid) {
                    $rootScope.systemNotification('Пропущено назву опитування', 'alert-warning', 4000);
                    return;
                }

                if (!self.poll.quests.length) {
                    $rootScope.systemNotification('Перелік питань не може бути пустим', 'alert-warning', 4000);
                    return;
                }

                self.poll.author = AuthService.getUserData().login;

                PollService.save(self.poll, function (data) {
                    if (data.success) {
                        $rootScope.systemNotification('Опитування збережено успішно', 'alert-success', 4000);
                        $state.go('main.list');
                    }
                }, function (err) {
                    $rootScope.systemNotification('Помилка збереження опитування: ' + err.message, 'alert-danger', 4000);
                })

            };

            /*
             * Help functions
             */
            function clearEditItemData() {
                self.editItem = null;
                self.editItemIndex = null;
            };

            function isQuesIndexExists(index) {
                if (typeof index === 'undefined' || typeof self.poll.quests[index] === 'undefined') {
                    return false;
                }

                return true;
            };

        }]);

angular.module('poll')
.controller('PollPassController', [
    '$rootScope','$state', 'PollService', 'AuthService', 'AnswerService', 'AnswerDeepService',
    function ($rootScope, $state, PollService, AuthService, AnswerService, AnswerDeepService) {

    var self = this;

    self.poll = {};
    self.pollId = null;
    self.nows = [];
    self.desirables = [];
    self.perspectives = [];

    if (!$state.params.pollId) {
        $state.go('main.list');
    } else {
        loadPoll();
    }

    self.saveAnswer = function() {
        var ans = getAnswerData();

        AnswerService.save(ans, function(data) {
            $state.go('main.list');
            $rootScope.systemNotification('Ваша відповідь збережена', 'alert-success', 4000);
        }, function(err) {
            $rootScope.systemNotification('Помилка збереження відповіді: ' + data.message, 'alert-danger', 4000);
        })
    };

    function loadPoll() {
        self.pollId = $state.params.pollId;

        PollService.get({id: self.pollId}, function (data) {
            self.poll = data.poll || {};

            if (self.poll.quests.length) {
                initAnswers(self.poll.quests.length);
            }
        }, function (err) {
            $rootScope.systemNotification('Помилка при завантаженні опитування: ' + data.message, 'alert-danger', 4000);
        });
    };

    function initAnswers(count) {
        var i;

        for(i = 0; i < count; i++) {
            self.nows.push({
                a: 50,
                b: 50,
                c: 50,
                d: 50
            });
        }

        self.desirables = angular.copy(self.nows);
        self.perspectives = angular.copy(self.nows);
    };

    function answerAverage(answers) {
        var len = answers.length, 
            ans = {
                a: 0,
                b: 0,
                c: 0,
                d: 0
            };

        answers.forEach(function(curr) {
            ans.a += curr.a;
            ans.b += curr.b;
            ans.c += curr.c;
            ans.d += curr.d;
        });

        ans.a = Math.round(ans.a / len);
        ans.b = Math.round(ans.b / len);
        ans.c = Math.round(ans.c / len);
        ans.d = Math.round(ans.d / len);

        return ans;
    };

    function getAnswerData() {
        var data = {},
            deepClass = '',
            user = AuthService.getUserData();

        data.pollId = self.pollId;
        data.user = {
            id: user.id,
            fullName: user.name + " " + user.surname,
            category: user.category
        };

        // level of scroll changes according to default answer value
        data.deep = AnswerDeepService.defineDeepChangesClass(self.nows, self.desirables, self.perspectives);

        data.answers = {
            nows: self.nows,
            desirables: self.desirables,
            perspectives: self.perspectives
        };

        data.answersAvg = {
            nows: answerAverage(self.nows),
            desirables: answerAverage(self.desirables),
            perspectives:  answerAverage(self.perspectives)
        }

        return data;
    };

}]);
angular.module('poll')
    .controller('PollsListController', ['PollService', function (PollService) {
        var self = this;

        self.polls = [];

        PollService.query(function (list) {
            if (angular.isArray(list)) {
                self.polls = list;
            }
        }, function (err) {
            console.log('Get Polls list Error ', err);
        });

        self.parseDate = function (dataString) {
            var day, month,
                date = new Date(dataString);

            day = date.getDate();
            if ( day < 10 ) {
                day = '0' + day;
            }

            month = date.getMonth()+1;
            if ( month < 10 ) {
                month = '0' + month;
            }

            return day + '-' + month + '-' + date.getFullYear();
        }
    }]);

angular.module('poll')
    .factory('AnswerDeepService',[function () {
    	var service = {},
    		// defuault poll answer value
    		defAnswer = 50,
            // defuault answers in poll question
            answersPerItem = 4,
    		deepChanges = {
    			deep0_10: {
    				min: 0,
    				max: 10,
                    text: "0-10"
    			},
    			deep11_20: {
    				min: 11,
    				max: 20,
                    text: "11-20"
    			},
    			deep21_30: {
    				min: 21,
    				max: 30,
                    text: "21-30"
    			},
    			deep31_40: {
    				min: 31,
    				max: 40,
                    text: "31-40"
    			},
    			deep41_50: {
    				min: 41,
    				max: 50,
                    text: "41-50"
    			}
    		};

    	function sumChanges(arr) {
    		var sum = 0,
                item = null,
    			len = arr.length;

    		for (var i = 0; i < len; i++) {
                item = arr[i];

    			for(var k in item) {
                    sum += Math.abs(item[k] - defAnswer);
                }
    		}

    		return sum;
    	};

        function getDeepLevel(val) {
            var deepLevel = '';

            for(var key in deepChanges) {
                if (deepChanges[key].min <= val && val <= deepChanges[key].max) {
                    deepLevel = key;
                    break;
                }
            }

            return deepLevel;
        };

    	// public methods
    	service.getDeeps = function() {
    		return deepChanges;
    	};

    	service.defineDeepChangesClass = function(nows, des, pres) {
            var res = 0,
                deepVal = 0;
                commonAnswersCount = 0;;

    		nows = nows || [];
    		des = des || [];
    		pres = pres || [];

            res += sumChanges(nows);
            res += sumChanges(des);
            res += sumChanges(pres);

            commonAnswersCount = (nows.length + des.length + pres.length) * answersPerItem;
            deepVal = Math.round(res / commonAnswersCount);

            return getDeepLevel(deepVal);
    	};

        return service;
    }]);
angular.module('poll')
    .factory('PollService',['$resource', function ($resource) {
        return $resource('polls/:id');
    }]);
angular.module('ceUtil')
    .directive('ceRangeSlider', ['$document', function ($document) {
        return {
            restrict: 'E',
            templateUrl: '/util/ce_range_slider.tpl.html',
            scope: {
                answers: '=',
                keyOne: '@',
                keyTwo: '@'
            },
            link: function ($scope, $element, $attrs) {
                var min = 0,
                    max = 100,
                    step = 1,
                    percent = 0,
                    sliderContainer = $element[0].querySelector('.ce-slider-container'),
                    sliderHighlightStyle = $element[0].querySelector('.ce-slider-bar-highlight').style,
                    slider = $element[0].querySelector('.ce-slider-handle'),
                    sliderHeight = slider.getBoundingClientRect().height;
                
                $scope.valueTwo = $scope.answers[$scope.keyTwo] || 0;
                $scope.valueOne = $scope.answers[$scope.keyOne] || 0;
                percent = $scope.valueTwo / max;

                $scope.moveSlider = function (per) {
                    $scope.$evalAsync(function () {
                        $scope.valueTwo = Math.round(max * per / step) * step;
                        $scope.valueOne = max - $scope.valueTwo;

                        $scope.answers[$scope.keyTwo] = $scope.valueTwo;
                        $scope.answers[$scope.keyOne] = $scope.valueOne;
                
                        sliderHighlightStyle.height = (per * 100) + '%';
                        slider.style.top = (per * 100) + '%';
                    });
                    
                };

                $scope.engageSlider = function(event) {
                    var startPosY = event.screenY,
                        basepercent = percent,
                        sliderContainerHeight = sliderContainer.getBoundingClientRect().height;
                    
                    event.preventDefault();

                    mouseMove = function (event) {
                        percent = basepercent + (event.screenY - startPosY) / (sliderContainerHeight - sliderHeight);
                        
                        if (percent < 0) {
                            percent = 0;
                        }

                        if (percent > 1) {
                            percent = 1;
                        }

                        $scope.moveSlider(percent);
                    };

                    mouseUp = function () {
                        $document.off("mousemove", mouseMove);
                        $document.off("mouseup", mouseUp);
                        
                         $scope.moveSlider(percent);
                    };

                    $document.on("mousemove", mouseMove);
                    $document.on("mouseup", mouseUp);
                }

                 $scope.moveSlider(percent);
            }
        }
    }]);

//# sourceMappingURL=main.js.map
