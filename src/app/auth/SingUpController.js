angular.module('app')
    .controller('SingUpController', [
        '$rootScope','$state', 'AuthService', 'categories',
        function ($rootScope, $state, AuthService, categories) {
        var self = this;

        self.categories = categories[0] ? categories[0].values : [];
        
        self.user = {
            login: '',
            password: '',
            name: '',
            surname: '',
            email: '',
            category: self.categories[0] ? self.categories[0].name : ''
        };

        self.singUpUser = function () {
            AuthService.signUp(self.user)
                .then(function (data) {
                        $state.go('main.default');
                    },
                    function (self) {
                        $rootScope.systemNotification('Перевірьте введені дані', 'alert-warning', 4000);
                    })
        };
    }]);