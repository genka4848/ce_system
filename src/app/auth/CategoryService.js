angular.module('app')
	.factory('CategoryService', ['$resource', function ($resource) {
		return $resource('category/:id', {id: '@id'}, {
			update: {
				method: 'PUT'
			},
			get: {
				method: 'GET',
				isArray: true
			},
			getTypes: {
				method: 'GET',
				url: '/types',
				isArray: true
			}
		});
	}]);