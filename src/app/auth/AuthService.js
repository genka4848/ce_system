angular.module('app')
    .factory('AuthService', ['$q', '$http', function ($q, $http) {
        var service = {},
            authorized = false,
            userStatusPromise = null,
            user = {};

        service.signIn = function (login, password) {
            var defer = $q.defer();

            $http.post('/login', {
                login: login,
                password: password
            }).success(function (data) {
                if (data.success) {
                    authorized = true;
                    user = data.user;
                }
                defer.resolve(user);
            }).error(function (error) {
                defer.reject(error);
            });

            return defer.promise;
        };

        service.signUp = function (userData) {
            var defer = $q.defer();

            $http.post('/register', userData)
                .success(function (data) {
                if (data.success) {
                    authorized = true;
                    user = data.user;
                }
                defer.resolve(user);
            }).error(function (error) {
                defer.reject(error);
            });

            return defer.promise;
        };

        service.userStatus = function () {
            var defer = $q.defer();

            if(userStatusPromise) {
                return userStatusPromise;
            }

            $http.post('/userinfo').success(function (data) {
                if (data.success) {
                    authorized = true;
                    user = data.user;
                }
                defer.resolve(user);
            }).error(function (error) {
                defer.reject(error);
            });

            userStatusPromise = defer.promise;

            return defer.promise;
        };

        service.logOut = function () {
            var defer = $q.defer();
            
            $http.post('/logout')
                .finally(function (err) {
                    authorized = false;
                    user = {};
                    defer.resolve(err);
                });

            return defer.promise;
        };

        service.isAccessByRole = function(param) {
            if (angular.isDefined(param.onlyAdmin)) {
                return user.role === 'admin';
            }

            return true;
        };

        service.isPrivateRoute = function() {
            return user.role === 'admin';
        };

        service.isAuthorized = function () {
            return authorized;
        };

        service.getUserData = function () {
            return angular.copy(user);
        };

        return service;
    }])
;