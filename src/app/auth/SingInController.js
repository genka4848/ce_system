angular.module('app')
    .controller('SingInController', ['$rootScope', '$state', 'AuthService', function ($rootScope, $state, AuthService) {
        this.login = '';
        this.password = '';

        this.loginUser = function () {
            AuthService.signIn(this.login, this.password)
                .then(function (resp) {
                        $state.go('main.default');
                    },
                    function (err) {
                        $rootScope.systemNotification('Перевірьте введені дані', 'alert-warning', 4000);
                    })
        };
    }]);