angular.module('category', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('main.categories', {
                url: 'categories',
                templateUrl: '/category_groups.tpl.html',
                controller: 'CategoryGroupController',
                controllerAs: 'categoryGroupCtrl',
                data: {
                    private: true,
                    onlyAdmin: true
                },
                resolve: {
                    groups: ['CategoryService', function(CategoryService) {
                        return CategoryService.query().$promise;
                    }]
                }
            })
            .state('main.item', {
                url: 'categories/:id',
                templateUrl: '/categories.tpl.html',
                controller: 'CategoryListController',
                controllerAs: 'categoryListCtrl',
                data: {
                    private: true,
                    onlyAdmin: true
                }
            });
    }]);
