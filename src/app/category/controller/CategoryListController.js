angular.module('category')
	.controller('CategoryListController', ['CategoryService', '$state', '$uibModal',
        function (CategoryService, $state, $uibModal) {

		var self = this;

		self.categories = []
        self.sumCoeff = 0;

        if (!$state.params.id) {
            $state.go('main.categories');
            return;
        }

        CategoryService.get({id: $state.params.id}, function(data) {
            self.categories = data[0].values;
            updateCoeff();
        });

		self.edit = function(index) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/modal/edit_category.tpl.html',
                controller: 'ModalEditCategoryCtrl',
                resolve: {
                    catIndex: function () {
                        return index;
                    },
                    categories: function() {
                        return self.categories;
                    },
                    calcCoeff: function() {
                        return calcCoeff;
                    },
                    groupId: function() {
                        return $state.params.id;
                    }
                }
            });

            modalInstance.result.then(function (category) {
	            self.categories[index] = category;
                updateCoeff();
			});
		};

        function updateCoeff() {
            self.sumCoeff = calcCoeff(self.categories);
        };

        function calcCoeff(categories) {
            var sum = 0;

            categories.forEach(function(cat) {
                sum += parseFloat(cat.coefficient);
            });

            sum = Math.round(sum * 1000) / 1000;

            return sum;
        };
	}]);