angular.module('category')
    .controller('ModalEditCategoryCtrl',['$scope', '$uibModalInstance', 'CategoryService', 'catIndex', 'categories', 'calcCoeff', 'groupId',
        function ($scope, $uibModalInstance, CategoryService, catIndex, categories, calcCoeff, groupId) {

        $scope.category = angular.copy(categories[catIndex]);
        $scope.categories = angular.copy(categories);

        $scope.ok = function () {
            var coef = null;


            if ($scope.editCategoryForm.$invalid) {
                return;
            }

            coef = parseFloat($scope.category.coefficient);
            if ((coef != $scope.category.coefficient) || coef < 0 || coef > 1) {
                return;
            }

            $scope.categories[catIndex] = $scope.category;

            if (!checkCoeff($scope.categories)) {
                $scope.systemNotification('Сума коефіцієнтів не має перевищувати 1', 'alert-danger', 4000);
                return;
            }

            CategoryService.update({id: groupId}, {
                values: $scope.categories
            }, function(){
                $uibModalInstance.close($scope.category);
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function checkCoeff(categories) {
            return calcCoeff(categories) <= 1;
        };
    }]);