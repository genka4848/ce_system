angular.module('category')
	.controller('CategoryGroupController', ['groups', function (groups) {
		var self = this;

		self.groups = groups;
	}]);