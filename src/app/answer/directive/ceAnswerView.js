angular.module('answer')
	.directive('ceAnswerView', [
		function() {
			return {
				restrict: 'E',
				scope: {},
				templateUrl: '/answer_view_directive.tpl.html',
				link: function(scope, element, attrs) {
					var clearData = [null, 0, null, 0, null, 0, null, 0],
						nows = angular.copy(clearData),
						desirables = angular.copy(clearData),
						perspectives = angular.copy(clearData);

					scope.answerInds = {
						a: 1,
						b: 7,
						c: 5,
						d: 3
					};

					scope.$on('updateView', function(event, data) {
						makeData(data);
					});

					scope.$on('clearView', function() {
						chart.series[0].setData(clearData);
						chart.series[1].setData(clearData);
						chart.series[2].setData(clearData);
					});

					function makeData(data) {
						for (var k in data.nows) {
							nows[scope.answerInds[k]] = data.nows[k];
						}

						for (var k in data.desirables) {
							desirables[scope.answerInds[k]] = data.desirables[k];
						}

						for (var k in data.perspectives) {
							perspectives[scope.answerInds[k]] = data.perspectives[k];
						}

						chart.series[0].setData(nows.splice(0, nows.length));
						chart.series[1].setData(desirables.splice(0, desirables.length));
						chart.series[2].setData(perspectives.splice(0, perspectives.length));
					};

					$('.answer-view-container', element).highcharts({
						chart: {
							polar: true,
							type: 'area'
						},
						title: false,
						credits: {
							enabled: false
						},
						pane: {
							startAngle: 0
						},
						xAxis: {
							categories: ['Гнучкість', '(A) Кланова, гнучка модель розвитку', 'Зовнішній', '(D) Ієрархія, консервативна модель розвитку', 'Контроль', '(C) Ринок, експансивна модель розвитку', 'Внутрішній', '(B) Адхократія, синергічна модель розвитку'],
							tickmarkPlacement: 'on',
							lineColor: '#4682B4',
							lineWidth: 1,
							gridLineColor: '#4682B4'
						},
						yAxis: {
							min: 0,
							max: 1,
							tickPositions: [0, 0.33, 0.67, 1],
							gridLineColor: '#4682B4'
						},
						tooltip: {
							shared: true
						},
						series: [{
							type: 'area',
							name: 'Зараз',
							connectEnds: true,
							connectNulls: true,
							fillOpacity: 0.15,
							pointPlacement: 'on',
							marker: {
								symbol: 'circle'
							},
							data: nows
						}, {
							type: 'area',
							name: 'Майбутнє',
							connectEnds: true,
							connectNulls: true,
							fillOpacity: 0.15,
							pointPlacement: 'on',
							marker: {
								symbol: 'circle'
							},
							data: desirables
						}, {
							type: 'area',
							name: 'Перспектива',
							connectEnds: true,
							connectNulls: true,
							fillOpacity: 0.15,
							pointPlacement: 'on',
							marker: {
								symbol: 'circle'
							},
							data: perspectives
						}]
					});

					var chart = $('.answer-view-container', element).highcharts()

					// end link
				}
			}
		}
	]);