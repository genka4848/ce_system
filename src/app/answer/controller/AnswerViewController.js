angular.module('answer')
	.controller('AnswerViewContoller', [
		'$rootScope', '$scope', '$state', 'AnswerService', 'AnswerDeepService', '$timeout', 'groups', 'meta',
		function($rootScope, $scope, $state, AnswerService, AnswerDeepService, $timeout, groups, meta) {
			var self = this,
				// timeout id selecting users ansers
			    timeoutId = null;

		    // answers by category
		    self.answers = [];

		    // all categories
		    self.groups = [];
		    self.group = [];
		    self.categories = [];

		    // special object for define category coeff
		    self.catIds = {};

		    // deep classes of answer changing
		    self.deepClasses = {};

		    // calculated answer with coeff
		    self.answer = {};

		    // users data
		    self.users = [];
		    self.totalFindedUsers = 0;

		    // search params
		    self.pollId = null;
		   	self.category = null;
		    self.year = null;
		    self.deep = null;
		    self.selectedUsers = [];

		    // define needs of reloading users list
		    self.needUsersReload = true;

		    // user filter name val
		    self.filterName = '';

			if (!$state.params.pollId) {
		        return;
		    } 

		    self.groups = groups;
		    self.group = self.groups[0] ? self.groups[0].values : [];
		    self.categories = angular.copy(self.group);

	    	self.pollId = $state.params.pollId;
	    	self.deepClasses = AnswerDeepService.getDeeps();
	    	createCatIds();
	        loadAnswersViewData();
	    

		    self.selectCat = function() {
		    	loadAnswersViewData();
		    };

		    self.selectGroup = function() {
		    	if (self.group) {
		    		self.needUsersReload = false;
		    		self.categories = self.group;
		    		resetAnswer();
		    		createCatIds();
		    		caclWithCoeff();
		    		$scope.$broadcast('updateView', self.answer);
		    	};
		    };


		    self.selectYear = function() {
		    	loadAnswersViewData();
		    };

		    self.selectDeep = function() {
		    	loadAnswersViewData();
		    };

		    self.clearSelect = function() {
			   	self.category = null;
			    self.year = null;
			    self.deep = null;
		    	loadAnswersViewData();
		    };

		    self.selectUsers = function() {
		    	self.needUsersReload = false;
		    	if (timeoutId) {
		    		$timeout.cancel(timeoutId);
		    		timeoutId = null;
		    	}

		    	timeoutId = $timeout(loadAnswersViewData, 700);
		    };

		    function loadAnswersViewData() {
		    	var params = {id: self.pollId};

		    	if (self.needUsersReload) {
	           		self.totalFindedUsers = 0;
	        		self.users = [];
	        		self.selectedUsers = [];
	           	}

		    	if(self.category) {
		    		params.category = self.category
		    	}

		    	if(self.year) {
		    		params.year = self.year;
		    	}

		    	if(self.deep) {
		    		params.deep = self.deep;
		    	}

		    	if (self.selectedUsers.length) {
		    		params.answers = self.selectedUsers;
		    	}

 				resetAnswer();
		        AnswerService.getViewData(params, function (result) {
		            self.answers = result.data;

		            if (self.answers.length) {
	    				caclWithCoeff();
		            	$scope.$broadcast('updateView', self.answer);
	    			} else {
	    				$scope.$broadcast('clearView');
	    			}

		        }, function (err) {
		            $rootScope.systemNotification('Помилка при завантаженні результатів відповідей: ' + data.message, 'alert-danger', 4000);
		        });
	    	};

	    	function createCatIds() {
	    		self.categories.forEach(function(item) {
	    			self.catIds[item.name] = item.coefficient;
	    		});
	    	};

	    	function caclWithCoeff() {
	    		self.answers.forEach(function(item) {

	    			if (self.needUsersReload) {
		           		self.totalFindedUsers  += item.total;
	    				self.users = self.users.concat(item.users);
		           	}

	    			sumGroups('nows', item);
	    			sumGroups('desirables', item);
	    			sumGroups('perspectives', item);
	    		});

	    		averangeGroups();

	    		self.needUsersReload = true;
	    	};

	    	function sumGroups(field, group) {
	    		var elems = self.answer[field],
	    			elemsGroup = group[field];

	    		for(var val in elems) {
	    			elems[val] += elemsGroup[val] * self.catIds[group.category];
	    		}
	    	};

	    	function averangeGroups() {
	    		for(var f in self.answer) {
	    			for(var d in self.answer[f]) {
	    				self.answer[f][d] = Math.round(self.answer[f][d]) / 100;	
	    			}
	    		}
	    	};

	    	function resetAnswer() {
	    		self.answer = {
		    		nows: {
			    		a: 0,
			    		b: 0,
			    		c: 0,
			    		d: 0
			    	},
			    	desirables: {
			    		a: 0,
			    		b: 0,
			    		c: 0,
			    		d: 0
			    	},
			    	perspectives: {
			    		a: 0,
			    		b: 0,
			    		c: 0,
			    		d: 0
			    	}
	    		};
	    	};
	}]);