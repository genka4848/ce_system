angular.module('answer', [])
	.factory('AnswerService', ['$resource', function ($resource) {
		return $resource('answer/:id', {id: '@id'}, {
			getViewData: {
				method: "POST",
				url: '/answer/data/:id',
				params: {
					category: '@category',
					year: "@year",
					deep: "@deep",
					answers: "@answers"
				}
			},
			getMetaData: {
				method: "GET",
				url: "/answer/meta/:id"
			}
		});
	}])
	.config(['$stateProvider', function($stateProvider) {
		$stateProvider
            .state('main.answerView', {
            	abstract: true,
                url: 'answer/view/:pollId',
                templateUrl: '/answer_view_test.tpl.html',
                controller: ['$state', 'meta', function($state, meta) {
                	var self = this;

					self.generalTotal = 0;
					self.years = [];
					self.isCompareMode = false;

                	if (!$state.params.pollId) {
		        		$state.go('main.list');
		        		return;
		        	}

		        	initMetaData();

		        	self.setComareMode = function() {
		        		self.isCompareMode = !self.isCompareMode;
		        	};

		        	function initMetaData() {
			    		var years = [];
			    		
			    		if (!meta) {
			    			return;
			    		}

			    		if(Array.isArray(meta.data) && meta.data[0]) {
			    			years = meta.data[0].years;
			    			self.generalTotal = meta.data[0].total;
			    		}
			    		
			    		years.sort(function(a, b) { return b - a; });

			    		self.years = years;
	    			};
                }],
                controllerAs: 'answerViewAbsCtrl',
                resolve: {
                    groups: ['CategoryService', function(CategoryService) {
                        return CategoryService.query().$promise;
                    }],
                    meta: ['AnswerService', '$stateParams', function(AnswerService, $stateParams) {
                    	if ($stateParams.pollId) {
                    		return AnswerService.getMetaData({id: $stateParams.pollId}).$promise;
                    	}
                        
                        return false;
                    }]
                }
            })
            .state('main.answerView.result', {
                url: '',
                views: {
                	'view1@main.answerView': {
                		templateUrl: '/answer_view.tpl.html',
                		controller: 'AnswerViewContoller',
                		controllerAs: 'answerViewCtrl',
                	},
                	'view2@main.answerView': {
                		templateUrl: '/answer_view.tpl.html',
                		controller: 'AnswerViewContoller',
                		controllerAs: 'answerViewCtrl',
                	}
                }
            })
	}]);