angular.module('messenger', [])
    .run(['$rootScope', function($rootScope) {
        $rootScope.systemNotification = function(message, type, delay) {
            if(angular.isDefined(message)) {
                $rootScope.$emit('showNotification', {
                    message: message,
                    type: type,
                    delay: delay
                })
            }
        }
    }])
    .directive('messengerNotification', ['$timeout', function($timeout) {
        return {
            restrict: 'E',
            templateUrl: '/modal/messenger.tpl.html',
            link: function($scope, $element) {
                var timeoutId = null, alert = null, msg = null,
                    defaultConfig = {
                        delay: 3000,
                        type: 'alert-success'
                    };

                alert = angular.element('.alert', $element);
                msg = angular.element('.message', $element);

                $scope.close = function() {
                    if (timeoutId) {
                        $timeout.cancel(timeoutId);
                        timeoutId = null;
                    }
                    alert.slideUp();
                };

                $scope.$on('showNotification', function(event, data) {
                    if (timeoutId) {
                        $scope.close();
                    }
                    $scope.alertType = data.type || defaultConfig.type;
                    msg.text(data.message);
                    alert.slideDown();
                    timeoutId = $timeout($scope.close, data.delay || defaultConfig.delay);
                });
            }
        }
    }]);