angular.module('ceUtil')
    .directive('ceRangeSlider', ['$document', function ($document) {
        return {
            restrict: 'E',
            templateUrl: '/util/ce_range_slider.tpl.html',
            scope: {
                answers: '=',
                keyOne: '@',
                keyTwo: '@'
            },
            link: function ($scope, $element, $attrs) {
                var min = 0,
                    max = 100,
                    step = 1,
                    percent = 0,
                    sliderContainer = $element[0].querySelector('.ce-slider-container'),
                    sliderHighlightStyle = $element[0].querySelector('.ce-slider-bar-highlight').style,
                    slider = $element[0].querySelector('.ce-slider-handle'),
                    sliderHeight = slider.getBoundingClientRect().height;
                
                $scope.valueTwo = $scope.answers[$scope.keyTwo] || 0;
                $scope.valueOne = $scope.answers[$scope.keyOne] || 0;
                percent = $scope.valueTwo / max;

                $scope.moveSlider = function (per) {
                    $scope.$evalAsync(function () {
                        $scope.valueTwo = Math.round(max * per / step) * step;
                        $scope.valueOne = max - $scope.valueTwo;

                        $scope.answers[$scope.keyTwo] = $scope.valueTwo;
                        $scope.answers[$scope.keyOne] = $scope.valueOne;
                
                        sliderHighlightStyle.height = (per * 100) + '%';
                        slider.style.top = (per * 100) + '%';
                    });
                    
                };

                $scope.engageSlider = function(event) {
                    var startPosY = event.screenY,
                        basepercent = percent,
                        sliderContainerHeight = sliderContainer.getBoundingClientRect().height;
                    
                    event.preventDefault();

                    mouseMove = function (event) {
                        percent = basepercent + (event.screenY - startPosY) / (sliderContainerHeight - sliderHeight);
                        
                        if (percent < 0) {
                            percent = 0;
                        }

                        if (percent > 1) {
                            percent = 1;
                        }

                        $scope.moveSlider(percent);
                    };

                    mouseUp = function () {
                        $document.off("mousemove", mouseMove);
                        $document.off("mouseup", mouseUp);
                        
                         $scope.moveSlider(percent);
                    };

                    $document.on("mousemove", mouseMove);
                    $document.on("mouseup", mouseUp);
                }

                 $scope.moveSlider(percent);
            }
        }
    }]);
