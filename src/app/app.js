angular.module('app', ['ui.router', 'ngResource', 'ui.bootstrap', 'ngAnimate', 'poll', 'answer', 'messenger', 'category', 'ceUtil'])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
            $stateProvider
                .state('main', {
                    abstract: true,
                    url: '/',
                    templateUrl: '/home.tpl.html',
                    data: {
                        private: true
                    },
                    controller: ['$scope', '$state', 'AuthService', 'CategoryService',
                    function ($scope, $state, AuthService, CategoryService) {
                        $scope.user = AuthService.getUserData();
                        $scope.categoryTitle = '';
                        $scope.categories = [];

                        $scope.logOut = function () {
                            AuthService.logOut()
                                .then(function () {
                                    $state.go('auth.login');
                                });
                        };

                        $scope.isPrivateRoute = AuthService.isPrivateRoute;

                        CategoryService.getTypes(function(categories) {
                            $scope.categories = categories[0].values;
                            getCategoryTitle($scope.user.category);
                            $scope.categories = [];
                        });

                        function getCategoryTitle(name) {
                            var i, len = $scope.categories.length;

                            for (i = 0; i < len; i++) {
                                if (name === $scope.categories[i].name) {
                                    $scope.categoryTitle = $scope.categories[i].title;
                                    break;
                                }
                            }
                        };
                    }]
                })
                .state('main.default', {
                    url: '',
                    templateUrl: '/default.tpl.html'
                })
                .state('auth', {
                    abstract: true,
                    templateUrl: '/auth_abstract.tpl.html'
                })
                .state('auth.login', {
                    url: '/login',
                    templateUrl: '/login_form.tpl.html',
                    controller: 'SingInController',
                    controllerAs: 'singInCtrl'
                })
                .state('auth.register', {
                    url: '/register',
                    templateUrl: '/register_form.tpl.html',
                    controller: 'SingUpController',
                    controllerAs: 'singUpCtrl',
                    resolve: {
                        categories: ['CategoryService', function(CategoryService) {
                            // return [];
                            return CategoryService.getTypes().$promise;
                        }]
                    }
                });

            $urlRouterProvider.otherwise("/");

            $locationProvider.html5Mode(false);
            $locationProvider.hashPrefix('!');

            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        }])
    .run(['$rootScope', '$state', 'AuthService', function ($rootScope, $state, AuthService) {
        var onAppRun = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            var initState = null;

            event.preventDefault();
            onAppRun();

            AuthService.userStatus()
                .then(function () {
                    if (AuthService.isAuthorized()) {
                        initState = (toState.name == 'auth.login' || toState.name == 'auth.register' || !AuthService.isAccessByRole(toState.data))
                            ? 'main.default' : toState;
                        $state.go(initState);
                    } else {
                        initState = (toState.data && toState.data.private ? 'auth.login' : toState);
                        $state.go(initState);
                    }
                }, function () {
                    $state.go('auth.login');
                });

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (AuthService.isAuthorized()) {
                    if (!toState.data || !toState.data.private || !AuthService.isAccessByRole(toState.data)) {
                        event.preventDefault();
                    }
                } else {
                    if (toState.data && toState.data.private) {
                        event.preventDefault();
                    }
                }

            });
        });
    }]);
