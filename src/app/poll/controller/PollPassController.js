angular.module('poll')
.controller('PollPassController', [
    '$rootScope','$state', 'PollService', 'AuthService', 'AnswerService', 'AnswerDeepService',
    function ($rootScope, $state, PollService, AuthService, AnswerService, AnswerDeepService) {

    var self = this;

    self.poll = {};
    self.pollId = null;
    self.nows = [];
    self.desirables = [];
    self.perspectives = [];

    if (!$state.params.pollId) {
        $state.go('main.list');
    } else {
        loadPoll();
    }

    self.saveAnswer = function() {
        var ans = getAnswerData();

        AnswerService.save(ans, function(data) {
            $state.go('main.list');
            $rootScope.systemNotification('Ваша відповідь збережена', 'alert-success', 4000);
        }, function(err) {
            $rootScope.systemNotification('Помилка збереження відповіді: ' + data.message, 'alert-danger', 4000);
        })
    };

    function loadPoll() {
        self.pollId = $state.params.pollId;

        PollService.get({id: self.pollId}, function (data) {
            self.poll = data.poll || {};

            if (self.poll.quests.length) {
                initAnswers(self.poll.quests.length);
            }
        }, function (err) {
            $rootScope.systemNotification('Помилка при завантаженні опитування: ' + data.message, 'alert-danger', 4000);
        });
    };

    function initAnswers(count) {
        var i;

        for(i = 0; i < count; i++) {
            self.nows.push({
                a: 50,
                b: 50,
                c: 50,
                d: 50
            });
        }

        self.desirables = angular.copy(self.nows);
        self.perspectives = angular.copy(self.nows);
    };

    function answerAverage(answers) {
        var len = answers.length, 
            ans = {
                a: 0,
                b: 0,
                c: 0,
                d: 0
            };

        answers.forEach(function(curr) {
            ans.a += curr.a;
            ans.b += curr.b;
            ans.c += curr.c;
            ans.d += curr.d;
        });

        ans.a = Math.round(ans.a / len);
        ans.b = Math.round(ans.b / len);
        ans.c = Math.round(ans.c / len);
        ans.d = Math.round(ans.d / len);

        return ans;
    };

    function getAnswerData() {
        var data = {},
            deepClass = '',
            user = AuthService.getUserData();

        data.pollId = self.pollId;
        data.user = {
            id: user.id,
            fullName: user.name + " " + user.surname,
            category: user.category
        };

        // level of scroll changes according to default answer value
        data.deep = AnswerDeepService.defineDeepChangesClass(self.nows, self.desirables, self.perspectives);

        data.answers = {
            nows: self.nows,
            desirables: self.desirables,
            perspectives: self.perspectives
        };

        data.answersAvg = {
            nows: answerAverage(self.nows),
            desirables: answerAverage(self.desirables),
            perspectives:  answerAverage(self.perspectives)
        }

        return data;
    };

}]);