angular.module('poll')
    .controller('PollCreateController', ['$rootScope', '$uibModal', '$state', 'PollService', 'AuthService',
        function ($rootScope, $uibModal, $state, PollService, AuthService) {

            var self = this;

            self.poll = {
                title: '',
                quests: []
            };

            self.editItem = null;
            self.editItemIndex = null;

            self.edit = function (index) {
                if (isQuesIndexExists(index)) {
                    self.editItem = self.poll.quests[index];
                    self.editItemIndex = index;

                    self.addQuestion();
                }
            };

            self.removeQuest = function (index) {
                if (isQuesIndexExists(index)) {
                    self.poll.quests.splice(index, 1);
                }
            };

            self.addQuestion = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/modal/create_quest.tpl.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        item: function () {
                            return self.editItem;
                        }
                    }
                });

                modalInstance.result.then(function (question) {
                    if (self.editItem) {
                        self.poll.quests[self.editItemIndex] = question;
                        clearEditItemData();
                    } else {
                        self.poll.quests.push(question);
                    }

                }, function () {
                    if (self.editItem) {
                        clearEditItemData();
                    }
                });
            };

            // TODO: add from validation, error classes
            self.save = function () {
                if (self.pollCreateForm.$invalid) {
                    $rootScope.systemNotification('Пропущено назву опитування', 'alert-warning', 4000);
                    return;
                }

                if (!self.poll.quests.length) {
                    $rootScope.systemNotification('Перелік питань не може бути пустим', 'alert-warning', 4000);
                    return;
                }

                self.poll.author = AuthService.getUserData().login;

                PollService.save(self.poll, function (data) {
                    if (data.success) {
                        $rootScope.systemNotification('Опитування збережено успішно', 'alert-success', 4000);
                        $state.go('main.list');
                    }
                }, function (err) {
                    $rootScope.systemNotification('Помилка збереження опитування: ' + err.message, 'alert-danger', 4000);
                })

            };

            /*
             * Help functions
             */
            function clearEditItemData() {
                self.editItem = null;
                self.editItemIndex = null;
            };

            function isQuesIndexExists(index) {
                if (typeof index === 'undefined' || typeof self.poll.quests[index] === 'undefined') {
                    return false;
                }

                return true;
            };

        }]);
