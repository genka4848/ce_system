angular.module('poll')
    .controller('PollsListController', ['PollService', function (PollService) {
        var self = this;

        self.polls = [];

        PollService.query(function (list) {
            if (angular.isArray(list)) {
                self.polls = list;
            }
        }, function (err) {
            console.log('Get Polls list Error ', err);
        });

        self.parseDate = function (dataString) {
            var day, month,
                date = new Date(dataString);

            day = date.getDate();
            if ( day < 10 ) {
                day = '0' + day;
            }

            month = date.getMonth()+1;
            if ( month < 10 ) {
                month = '0' + month;
            }

            return day + '-' + month + '-' + date.getFullYear();
        }
    }]);
