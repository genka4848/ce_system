angular.module('poll')
    .controller('ModalInstanceCtrl',['$scope',  '$uibModalInstance', 'item', function ($scope, $uibModalInstance, item) {
        $scope.question = {
            title: '',
            answers: {
                a: '',
                b: '',
                c: '',
                d: ''
            }
        };
        
        if (item) {
            $scope.question = angular.copy(item);
        }
        
        $scope.ok = function () {
            if($scope.createQuestForm.$valid) {
                $uibModalInstance.close($scope.question);
            } else {
                $scope.systemNotification('Дані невірні', 'alert-warning', 4000);
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);