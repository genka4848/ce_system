angular.module('poll')
    .factory('AnswerDeepService',[function () {
    	var service = {},
    		// defuault poll answer value
    		defAnswer = 50,
            // defuault answers in poll question
            answersPerItem = 4,
    		deepChanges = {
    			deep0_10: {
    				min: 0,
    				max: 10,
                    text: "0-10"
    			},
    			deep11_20: {
    				min: 11,
    				max: 20,
                    text: "11-20"
    			},
    			deep21_30: {
    				min: 21,
    				max: 30,
                    text: "21-30"
    			},
    			deep31_40: {
    				min: 31,
    				max: 40,
                    text: "31-40"
    			},
    			deep41_50: {
    				min: 41,
    				max: 50,
                    text: "41-50"
    			}
    		};

    	function sumChanges(arr) {
    		var sum = 0,
                item = null,
    			len = arr.length;

    		for (var i = 0; i < len; i++) {
                item = arr[i];

    			for(var k in item) {
                    sum += Math.abs(item[k] - defAnswer);
                }
    		}

    		return sum;
    	};

        function getDeepLevel(val) {
            var deepLevel = '';

            for(var key in deepChanges) {
                if (deepChanges[key].min <= val && val <= deepChanges[key].max) {
                    deepLevel = key;
                    break;
                }
            }

            return deepLevel;
        };

    	// public methods
    	service.getDeeps = function() {
    		return deepChanges;
    	};

    	service.defineDeepChangesClass = function(nows, des, pres) {
            var res = 0,
                deepVal = 0;
                commonAnswersCount = 0;;

    		nows = nows || [];
    		des = des || [];
    		pres = pres || [];

            res += sumChanges(nows);
            res += sumChanges(des);
            res += sumChanges(pres);

            commonAnswersCount = (nows.length + des.length + pres.length) * answersPerItem;
            deepVal = Math.round(res / commonAnswersCount);

            return getDeepLevel(deepVal);
    	};

        return service;
    }]);