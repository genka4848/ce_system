angular.module('poll')
    .factory('PollService',['$resource', function ($resource) {
        return $resource('polls/:id');
    }]);