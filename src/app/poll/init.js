angular.module('poll', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('main.list', {
                url: 'list',
                templateUrl: '/polls.tpl.html',
                controller: 'PollsListController',
                controllerAs: 'pollListCtrl'
            })
            .state('main.create', {
                url: 'create',
                templateUrl: '/create_poll.tpl.html',
                controller: 'PollCreateController',
                controllerAs: 'pollCreateCtrl',
                data: {
                    private: true,
                    onlyAdmin: true
                }
            })
            .state('main.pass', {
                url: 'pass/:pollId',
                templateUrl: '/poll_pass.tpl.html',
                controller: 'PollPassController',
                controllerAs: 'pollPassCtrl'
            });
    }]);
