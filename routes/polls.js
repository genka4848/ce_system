var express = require('express');
var router = express.Router();
var PollCtrl = require('../controllers/poll');

router.all('/polls*', function(req, res, next) {
     if (!req.isAuthenticated()) {
        res.json({success: false, message: 'NOT_LOGGED'});
        return;
    }

    next();
});


router.post('/polls', PollCtrl.create);

router.get('/polls', PollCtrl.getList);
router.get('/polls/:id', PollCtrl.get);

module.exports = router;