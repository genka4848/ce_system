var express = require('express');
var passport = require('passport');

var router = express.Router();
var AuthCtrl = require('../controllers/auth');

router.post('/login', AuthCtrl.logIn);
router.post('/register', AuthCtrl.singUp);
router.post('/logout', AuthCtrl.logOut);
router.post('/userinfo', AuthCtrl.userInfo);

module.exports = router;
