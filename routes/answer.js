var express = require('express');
var router = express.Router();

var Answer = require('../controllers/answer');

router.all('/answer*', function(req, res, next) {
     if (!req.isAuthenticated()) {
        res.json({success: false, message: 'NOT_LOGGED'});
        return;
    }

    next();
});

router.post('/answer', Answer.create);
router.post('/answer/data/:id', Answer.agregateData);

router.get('/answer/meta/:id', Answer.getMetaData);

module.exports = router;