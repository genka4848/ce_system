var express = require('express');
var router = express.Router();

var CategoryCtrl = require('../controllers/user-category');


router.get('/types', CategoryCtrl.getTypes);

router.all('/category*', function(req, res, next) {
     if (!req.isAuthenticated()) {
        res.json({success: false, message: 'NOT_LOGGED'});
        return;
    }

    next();
});
router.get('/category', CategoryCtrl.getList);
router.get('/category/:id', CategoryCtrl.getItem);

router.put('/category/:id', CategoryCtrl.update);

module.exports = router;
